.EXPORT_ALL_VARIABLES:

CRIO_VERSION ?= 1.25
KUBERNETES_VERSION ?= 1.25.12-00
ACCELERATOR ?= kvm

.PHONY: all init clean

all: init ubuntu.img

init:
	packer init template.pkr.hcl

clean:
	rm -rf packer.key* userdata.cfg cloud-init.img output-qemu/ ubuntu.img SHA256SUMS

packer.key:
	ssh-keygen -q -N "" -t ecdsa -f $@

userdata.cfg: packer.key userdata.cfg.in
	sed "s|%packer_ssh_pub%|`cat $<.pub`|g" $@.in > $@

cloud-init.img: userdata.cfg
	cloud-localds $@ $?

output-ubuntu/ubuntu: template.pkr.hcl cloud-init.img install.sh
	packer build template.pkr.hcl

ubuntu.img: output-ubuntu/ubuntu
	mv $< $@
	sha256sum $@ > SHA256SUMS



