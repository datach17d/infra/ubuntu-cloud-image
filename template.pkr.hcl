packer {
  required_plugins {
    qemu = {
      source  = "github.com/hashicorp/qemu"
      version = "~> 1"
    }
  }
}

variable "ACCELERATOR" {
  type    = string
  default = "${env("ACCELERATOR")}"
}

variable "CRIO_VERSION" {
  type    = string
  default = "${env("CRIO_VERSION")}"
}

variable "KUBERNETES_VERSION" {
  type    = string
  default = "${env("KUBERNETES_VERSION")}"
}

source "qemu" "ubuntu" {
  accelerator          = "${var.ACCELERATOR}"
  communicator         = "ssh"
  cpus                 = 4
  disk_compression     = true
  disk_image           = true
  disk_interface       = "virtio"
  disk_size            = "6000M"
  format               = "qcow2"
  iso_checksum         = "file:https://cloud-images.ubuntu.com/jammy/current/SHA256SUMS"
  iso_url              = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
  memory               = 4096
  net_device           = "virtio-net"
  qemuargs             = [["-cdrom", "cloud-init.img"]]
  ssh_password         = "ubuntu"
  ssh_private_key_file = "./packer.key"
  ssh_timeout          = "300s"
  ssh_username         = "packer"
  use_default_display  = true
  vm_name              = "ubuntu"
}

build {
  sources = ["source.qemu.ubuntu"]

  provisioner "shell" {
    environment_vars = ["CRIO_VERSION=${var.CRIO_VERSION}", "KUBERNETES_VERSION=${var.KUBERNETES_VERSION}"]
    execute_command  = "chmod +x {{ .Path }}; sudo {{ .Vars }} {{ .Path }}"
    script           = "./install.sh"
  }

}
