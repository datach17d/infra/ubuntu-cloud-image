#!/bin/bash -xe

UBUNTU_VERSION=xUbuntu_$(lsb_release -s -r)
CRIO_VERSION=${CRIO_VERSION:-1.22}
KUBERNETES_VERSION=${KUBERNETES_VERSION:-1.22.2-0}

echo "crio version: ${CRIO_VERSION}, kubernetes version: ${KUBERNETES_VERSION}"

export DEBIAN_FRONTEND=noninteractive

# enable netfilter for bridges module
echo "br_netfilter" > /etc/modules-load.d/bridge.conf
# enable iscsi_tcp for longhorn
echo "iscsi_tcp" > /etc/modules-load.d/iscsi.conf
systemctl restart systemd-modules-load.service

# enable netfilter for bridges and ip forwarding
cat << EOF > /etc/sysctl.d/98-kubeadm.conf
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

# https://github.com/cilium/cilium/issues/10645
echo 'net.ipv4.conf.lxc*.rp_filter = 0' > /etc/sysctl.d/99-override_cilium_rp_filter.conf

# https://github.com/cilium/cilium/issues/18706#issuecomment-1031470456
sed -i "s|#ManageForeignRoutingPolicyRules.*|ManageForeignRoutingPolicyRules=no|g" /etc/systemd/networkd.conf

systemctl restart systemd-sysctl

apt-get update
apt-get install -y linux-modules-extra-$(uname -r) linux-headers-$(uname -r) \
                   bridge-utils net-tools gcc \
                   apt-transport-https ca-certificates curl gnupg-agent \
                   software-properties-common \
                   nfs-common

apt-get remove -y unattended-upgrades

echo hpsa > /etc/initramfs-tools/modules
update-initramfs -u

# install crio, kubelet, kubeadm and kubectl
curl -fsSL https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable//${UBUNTU_VERSION}/Release.key | apt-key add -
curl -fsSL https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/${CRIO_VERSION}/${UBUNTU_VERSION}/Release.key | apt-key add -
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/${UBUNTU_VERSION}/ /"
add-apt-repository "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/${CRIO_VERSION}/${UBUNTU_VERSION}/ /"
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
add-apt-repository "deb https://apt.kubernetes.io/ kubernetes-xenial main"

apt-get -o Dpkg::Options::=--force-confdef install -y cri-o cri-o-runc \
                                                      kubelet=${KUBERNETES_VERSION} \
                                                      kubeadm=${KUBERNETES_VERSION} \
                                                      kubectl=${KUBERNETES_VERSION}

# enable services
systemctl enable --now iscsid
systemctl enable --now crio.service
kubeadm config images pull --cri-socket /var/run/crio/crio.sock
systemctl enable --now kubelet

# Don't need the crio cni for kubernetes
rm -f /etc/cni/net.d/*.conf

# Increase PID limit for crio
cat <<EOF > /etc/crio/crio.conf
[crio]
[crio.runtime]
pids_limit = 32767
EOF

apt-get clean
cloud-init clean
# Remove machine-id for DHCP so that hosts don't get same ip-address
echo -n > /etc/machine-id
userdel --remove --force packer
